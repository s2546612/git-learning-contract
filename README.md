# Git Learning Contract
## Introduction
This project is a learning project, meant to help me learn working with git and to adminstrate a gitlab project.
The README is here to help you navigate the project, making it easy to check if the project has been conform the proposed criteria for grading.
This README-file is written in accordance with the GitHub guide https://guides.github.com/features/mastering-markdown/
concerning .md (markdown) files.

## How to check the grading criteria
In the grading critera section of the (updated) learning contract it is stated that completion of the learning steps requires concrete proof.
In this README I have created a section called "Proof of the "learning steps"". It can be found below and it tells you what the proof of completion
is for each Learning step. The learning steps and their completion have been listed individually. Finally, the README contains a "How to git; improvements" section.
As already stated before, I am trying to learn to work with git(lab). Because of this the first time I try something it may not work or cause problems. Nevertheless
I provide the entire process of me trying to create the process so that it is clear why the project looks a certain way. This is however not always a good way to do it.
For example: just now I pushed the first commit and I got a error concerning the upstream-master. The mistakes I made are included in the proof but are filtered out in the
"How to git; improvements!" section. 


## Proof of the "learning steps"
Learning step | Proof of Completion
--------------|---------------------
L.S. 1 | You are currently viewing the repository on gitlab
L.S. 2 | In the png-file named "git init - added remote origin - configured user name - configured user email.png" you can see that everything has been set up as required. When I cloned using the https-method I had to authenticate myself
L.S. 3 | "git first push attempt.png" shows me successfully pushing to origin master. See also the "How to git; improvements!" section. "git second push.png"shows the improvement and a newer version of the README (compare to "First README version contain [sic] some errors.png"
L.S. 4 | We are now both editing this exact same line. Merge conflict!

## How to git; improvements!
L.S. 3
1. git add . (adds all files that are changed (or created) in order to add them to the commit)
1. git commit -m "Message starting with a capital, not ending with a dot" (commits all added files and creates a commit-message for gitlab)
1. git push --set-upstream origin master (when pushing the commit(s), you want to specify where to push to. We have configured git to push
to the origin, namely the git-learning-conctract repository on gitlab, and then in specific to the branch "master")